import { useParams } from "react-router";
import { useEffect, useState } from "react";
import {Col, Container, ProgressBar, Row, Spinner, Table} from "react-bootstrap";


const Item = ({page}) => {
  const {name} = useParams();
  const [loading, setLoading] = useState(true);
  const [pokemon, setPokemon] = useState({});

  useEffect(() => {

    const fetchdata = async () => {
      const response = await page(name);
      setPokemon(response);
      setLoading(false);
    }
    fetchdata();
  }, [])

  if (loading === true){
    return (
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
    );
  }

  return (
      <div className={"p t-4 " + pokemon.types[0].type.name}>
        <Container>
          <span className={"pokemon-name"}>{pokemon.name}</span>

          <Row>
            <Col className="Front"><img src={pokemon.sprites.front_default} alt={"front"}/></Col>
            <Col className="Back"><img src={pokemon.sprites.back_default} alt={"back"}/></Col>
          </Row>
          <Row>
            {pokemon.types.map((type, index) => {
              return <Col key={index} className={type.type.name + " type"}>{type.type.name}</Col>
            })}
          </Row>
          <div style={{marginTop: '40px'}}>
            <Table responsive>
              <tbody>
              <tr>
                <td>Weight</td>
                <td>{pokemon.weight} lb</td>
              </tr>
              <tr>
                <td>Height</td>
                <td>{pokemon.height}"</td>
              </tr>
              </tbody>
            </Table>
          </div>
          <Col className="Back"><img src={pokemon.sprites.other['official-artwork'].front_default} alt={"official"}/></Col>
          <h2>Stats</h2>

          <div style={{marginTop: '40px'}}>
            <Table>
              <tbody>
              {pokemon.stats.map((stat, index) => <tr key={index}>
                <td>{stat.stat.name}</td>
                <td><ProgressBar now={100*stat.base_stat/255} /></td>
                <td>{stat.base_stat}</td>
              </tr>)}
              </tbody>
            </Table>
          </div>
          <h2>Moves</h2>

          <div style={{marginTop: '40px'}}>
            <Table>
              <tbody>

              {pokemon.moves.map((move, index) => {
                if (index < 3){
                  return(
                      <tr key={index}>
                    <td>{move.move.name}</td>
                  </tr>
                  )
                }
              })}
              </tbody>
            </Table>
          </div>
        </Container>

      </div>
  );
}

export default Item;