import {Card, Col, Container, Row} from "react-bootstrap";



const List = ({ list = [] }) => {
    if (list.length === 0) {
      return <div>No pokémon found</div>;
    }

    return (
        <div className={"mt-4"}>
            <Container>
                <Row className={"position-relative"}>
                {list.map((pokemon, index) =>
                    <Col  key={index} className={"mt-4  col-2"}>
                        <a href={`/pokemon/${pokemon.name}`}>
                            <Card >
                                <Card.Img variant="top" src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id}.png`} />
                                <Card.Body>
                                    <Card.Title className={"text-center pokemon-name-list"}>{pokemon.name}</Card.Title>
                                </Card.Body>
                            </Card>
                        </a>
                    </Col>

                )}
                </Row>
            </Container>

        </div>
    );
  };
  
  export default List;