import {Container, FormControl, InputGroup} from "react-bootstrap";


const Form = ({ name, handleChange }) => (
    <div  className={"mt-4"}>
        <Container>
            <InputGroup>
                <InputGroup.Text id="basic-addon1">Filter</InputGroup.Text>
                <FormControl
                    placeholder="Pokemon"
                    aria-label="Pokemon"
                    aria-describedby="basic-addon1"
                    defaultValue={name}
                    onChange={handleChange}
                />
            </InputGroup>
        </Container>
    </div>
  );
  
  export default Form;
  