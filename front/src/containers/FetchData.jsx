import React, { Component } from "react";

import FilterPokemon from "./FilterPokemon";
import {Spinner} from "react-bootstrap";

class FetchData extends Component {
  constructor() {
    super();

    this.fetchData = this.fetchData.bind(this);
  }

  state = { error: false, loading: true, data: "Loading...", pokemon:{} };

  async fetchData() {
    try {
      const response = await fetch(
        `https://pokeapi.co/api/v2/pokemon?limit=889&offset=0`
      );
      const data = await response.json();

      data.results.map((pokemon, index) => {
        pokemon.id = index+1;
        return pokemon
        
    }
    )


      this.setState({ loading: false, pokemon: data });
    } catch (err) {
      this.setState({ error: true });
      throw err;
    }
  }
  componentDidMount() {
    this.fetchData();
  }

  render() {
    if (this.state.error) {
      return <div>ERROR</div>;
    }

    if (this.state.loading) {
      return (
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
      );
    }
    
    return (<FilterPokemon></FilterPokemon>);
  }
}

export default FetchData;