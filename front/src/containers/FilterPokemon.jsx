import { useContext } from "react";
import { List, Form } from "../components";

import MainContext from "../contexts/MainContext";
import {Spinner} from "react-bootstrap";

const FilterPokemon = () => {
  const { filtered, handleChange, name, data } = useContext(MainContext);
  if (data.length === 0){
      return (
          <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
          </Spinner>
      )
  }

  return (
    <>
      <Form name={name} handleChange={handleChange}/>
      <List list={filtered} />
    </>
  );
};

export default FilterPokemon;
