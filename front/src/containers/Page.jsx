
import React, { Component } from "react";
import {Item} from "../components"


class Page extends Component {
    constructor() {
    super();
    this.page = this.page.bind(this);

    }

    async page(name){
        const response = await fetch(
            `https://pokeapi.co/api/v2/pokemon/${name}`
        )
        const data = await response.json();
        return data
    }

    
    render() {
        return (
            <Item page={this.page}/> 
        );
    }
}

export default Page;