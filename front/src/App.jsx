import './App.css';
import React, {Component} from "react";
import FetchData from "./containers/FetchData";
import { Routes, Route } from "react-router-dom";
import Page from "./containers/Page";
import FilterPokemon from './containers/FilterPokemon';
import {Navbar, Container, Nav} from "react-bootstrap";


class App extends Component {
  render() {
    return (
      <div>
        <header className={"sticky-top"}>
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="/">Pokedex App</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="/pokemon">Tout les pokémons</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </header>
        <Routes>
          <Route path="/pokemon" element={<FetchData/>} />
          <Route path="/pokemon" element={<FilterPokemon></FilterPokemon>} />

          <Route path="/pokemon/:name" element={<Page/> } />
        </Routes>
      </div>
    );
  }
}

export default App;
