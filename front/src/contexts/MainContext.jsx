import { createContext, useState, useEffect } from "react";


const MainContext = createContext({});

const Provider = ({ children }) => {
  const [data, setData] = useState([]);
  const [name, setName] = useState("");
  const [filtered, setFiltered] = useState(data);

  const handleChange = (event) => {
    const inputValue = event.target.value;

    setName(inputValue);
    setFiltered(
        data.filter((pokemon) =>
        pokemon.name.toLowerCase().includes(inputValue.toLowerCase())
      )
    );
  };
 useEffect(() => {
    const fetchPokemon = async () => {
        const allPokemons = await fetch(
            `https://pokeapi.co/api/v2/pokemon?limit=889&offset=0`
        )
        const waitPokemon = await allPokemons.json();

        waitPokemon.results.map((pokemon, index) => {
            pokemon.id = index+1;
            return pokemon
        }
        )
        setFiltered(waitPokemon.results);
        setData(waitPokemon.results);

    }
    fetchPokemon()
    
  }, [])

  return (
    <MainContext.Provider value={{ data, handleChange, name, filtered}}>
      {children}
    </MainContext.Provider>
  );
};

export { Provider };
export default MainContext;
